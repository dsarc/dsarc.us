---
layout: post
title: '2020-01-09 Meeting Minutes'
date: '2020-01-09 00:00:00'
tags:
- minutes
---

### Do Something Amateur Radio Club  
### Latah County Auxiliary Communications Team  
### January 7th, 2020  

#### **_Attendees_**    
* Micheal
* Autumn St.Amand KI7UTG
* Evan Hart, KG7WPM
* Dahmen Garner, KI7SIJ
* Bill Ward, K9GRZ
* Dave Schumacher, KF7CSV
* Sandee Schumacher, KA9UNA
* Jim Fielder
* Austin Cole, KF7SIW
* Nick Hewes, AF7ZJ

* Present

#### **_Treasurer’s Report_** 
* Item

#### **_Old Business_**  
* 2nd Jan meeting:  Micheal, What is DMR
* 2nd Feb meeting:  Micheal, How to set up and use DMR
* 2nd March Meeting: Fielder:  digital modes and MMDMV
* 1st Feb meeting:  Bill, CTCSS, DCS, etc

* Winter Field Day, Jan 25 and 26
    * AuxComm trailer—stage Friday night
    * Mountain View Park—Nick will check with Gary at Parks and Rec
    * Fair grounds is the alternate
    * Food:  $100/day available of club funds.  Fast food or whatever
    * Logging software:  FT log is supposed to be updated for WFD.  Nick will test and verify.  Backup is N1MM on Windows

#### **_New Business_**  
* Ham shack at fairgrounds
    * Need to develop a scope of the project so we know what we are trying to accomplish.  
    * Should get the tower installed this summer.  
  
* Officer election/selection:  wait a couple of months and figure it out
* Nick will draft standing rules and send them to the exec committee

* Michael has an i-gate for APRS but we need a digi-peater on a mountain.  Jim will see what he can find out via some of his contacts

* Austin is going to try and get the courthouse packet node operational in the next couple of months and we’ll see if we can put the UHF DMR repeater (belonging to Evan and Michael) as part of the install until the Moscow Mtn repeater is operational.
