---
layout: post
title: '2018-03-06 Meeting Minutes'
date: '2018-03-07 21:34:00'
tags:
- minutes
---

#### DSARC Meeting
#### March 06, 2018

** Present: **  
Nick Hewes, AF7ZJ  
Bill Ward, K9GRZ  
Chad Snyder, AE7BZ  
Ty Williams, W7TML  
Bill Magnie, KI7TSV  
Evan Hart, KG7WPM  
Christine Berven, KI7UTL  
Micheal Sotolongo, KE7UDV  
Demetre Nicholas, pending  
Parker Gibson, KI7UTI  
Dave Schumacher, KF7CSV  
Pat Blount, KE7SYF  
Autumn St.Amand KI7UT?  

* Absent  
* Austin Cole, KF7SIW  
* Heather Cole, KG7BUB  
* Glenn Boudreaux, KA5PTG  
* Tom Storer, KI6DER  
* Kelly Blackmon, NA5XX  
* Bill Duncan, KE7OVE  

_Treasurer’s Reports_ 
* DSARC: NA
* AuxComm: $255

_AuxComm_ 
* Discussion of what AuxComm is for the new members.  
* Website: https://latahaux.com/

_DSARC_ 
* Net participation.  Please try to make it to at least one of the three local emergency/ARES nets each week.  More is better!  
* March 31, 2018, Washington EOC to EOC 5th Saturday exercise.  We have been invited to play.  Focus will be HF digital with RMS Express.  Austin will get RMS Express running on the new county computer.  Bill will let them know that we’ll play, too.  
*  Tri-State ARO  meeting is in March.  We should show up as a show of support to that organization.  $15 annual dues, donations for the repeater fund are always welcome. Send email to <kc7qcs@arrl.com> 

_Upcoming Training:_
* Tech Class, March 24 and 31 in Clarkston.  Field exercise April 14 at Hell’s Gate.  Antenna build party at Hell’s Gate ARC April 19.

_Event Updates_
* After class Field exercise. Spring Valley style at Arboretum. March 3. Exercise went well with 10 nubies and 9 facilitators.  

* Riggins Jet Boat Races April 20 – 22.
If there are improvements that people can suggest for the training and operations of the hams at the jet boat races, please send the information to Ty.

* Bill W. contacted TIM RYNEARSON <N7UBO@msn.com>  to see how people can sign up for helping at the Riggins races.  For the first time in memory, the roster is full—we have as many hams as we “need.”  That being said, if anyone still wants to go please contact Tim directly.  He can find a good use for additional radio operators—and it’s a great time and a great way to gain experience for this type of event.
