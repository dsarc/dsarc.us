---
layout: post
title: '2017-11-07 Meeting Minutes'
date: '2017-11-07 21:00:00'
tags:
- minutes
---

#### DSARC Meeting
November 7, 2017

* Present:
    * Nick Hewes, AF7ZJ
    * Brett Copperi, KG7WVX
    * Bill Ward, K9GRZ
    * Austin Cole, KF7SIW
    * Evan Hart, KG7WPM
    * Michael Sotolongo, KE7UDV
    * James McElroy, Pending
    * Geoff Billin, KC7QCS
    * Jim Kuznir, K7LL

* Current Balance: $246.85

* NO tech class for November

* Arboretum Exercise, November 11, 2017
    * Setup 9 to 12
    * Exercise starts at noon
    * South end of new Arboretum
    * Approx. 20 stations

* Austin has updated the DSARC and AuxComm websites.

* AuxComm Updates
    * Bill went over the terms and conditions for AuxComm members as posted on the website.  Applications and background check waiver.  Applications and certificates of completion for ICS classes return to Bill, background check waivers must be notarized and taken to the sheriff’s office dispatch.  Bill will find out if SAR cards are acceptable as an alternate to a new background check.

* Future Labs:  
    * Bring in radios, TNCs, and computers and get packet messaging working.  One or two sessions on packet procedures.  Leading up to an EOC exercise in late winter.

* November 21 meeting will be held as scheduled.  

* December 19 meeting will be setting up for packet messages.  Bring radio, TNC, and computer and we’ll try to get things working.