---
layout: post
title: '2019-04-02 Meeting Minutes'
date: '2019-04-04 00:00:00'
tags:
- minutes
---

### Do Something Amateur Radio Club  
### Latah County Auxiliary Communications Team  
### April 4th, 2019  

#### **_Attendees_**    
* Nick Hewes, AF7ZJ
* Bill Ward, K9GRZ
* Katie Ward, KG7JHA
* Austin Cole, KF7SIW
* Heather Cole, KG7BUB
* Bill Magnie, KI7TSV
* Evan Hart, KG7WPM
* Micheal Sotolongo, KE7UDV
* Dave Schumacher, KF7CSV
* Autumn St.Amand KI7UTG
* Mike Nealon WA7MGN
* Sandee Schumacher, KA9UNA
* Dahmen Garner, KI7SIJ
* Matt Gee, KT7MLG
* Mike Brown, W7ACP
* Bob Schmokel, KJ7ASW
* Bart Gableman, N1BAG
* Jim Morrison, N2JM
* Marlen Morrison, KF7GIT
* Scott, Hammar

* Present

#### **_Treasurer’s Report_** 
* DSARC Finances:  $330 and change.  Dues are due.

#### **_Old Business_**  
**501(c)3 update.**  
* Mike, the attorney, will start drafting the paperwork.  A total of about $800 will be needed.  Donations are encouraged!  February update:  Mike has gotten us listed as an Idaho non-profit corporation.  He is now working on getting us the federal 501(c)3 designation.  Update:  we got billed and we need $825 to pay the bill.  We have an EIN and are an Idaho non-profit and donations can be tax deducted based on the expectation of 501(c)3 status.
* Austin is working on getting a PayPal account  as soon as the 501 (c)3 comes thru.

**Events** 

* _ACES training:_  February 23 and 24.  Bill, K9GRZ, is point person.  Update:  went fairly well, got done early!  13 people completed the class.  Bill will send in the ICS cert and exam packet on Friday.  Update:  packet sent in, no response, yet.

* _Packet Training:_  April 6.  Start at 2B for a short refresher then split between 2B and trailer for practical work.  Will try to do P2P, node, and gateway.  Need to test Pat with Winlink.

* _Tech Class:_  March 30 and 31.    Update:  six new techs!  We have 3 who signed up but were unable to attend due to illness.  Looking at May 18 and 19 for another class.  ==>Have the class participants been added to the DSARC email list?  Or should I send them an email pointing to the website and ask them to sign up?  


**Proposed Training opportunities:**

* Trainings at meetings, date TBD:
* Table Top Exercise on how we get deployed.
* ICS 100, 200, 700, 800:  What’s in it for me?  Why do we need to take those courses when we don’t seem to use them?
* Upcoming/potential projects and exercises.  
* We need to do some nighttime testing with HF.  We also need to find a way to establish a VHF/UHF link to Spokane and other surrounding areas.  
* Event idea:  do a non-repeater exercise to determine who can talk to who on what equipment.

**AuxComm/ARES**

* AuxComm dues are due.
* New membership category “student” for full time students.  Dues are waived for student members.  
* Three new members: 
    * Matt Gee, KT7MLG (congrats on passing Extra over the weekend!)
    * Dahmen Garner, KI7SIJ
    * Katie Ward, KG7JHA

#### **_New Business_**  
**There is a new email list sign up option on the DSARC website.  Please go sign up!**

WE NEED MEETING TOPICS AND TRAINING IDEAS!  

Next meeting:  SWR meters and antenna tuners.

**Meeting Topic – Bart**

Participated in the Washington 5th Saturday EOC exercise in Clarkston last weekend.  The exercise was using packet on VHF/UHF.  They did some experimenting with finding new nodes and hopping between them.  They have a packet chat room on Sunday evening.  They have a BBS set up for sharing information.  Maps of local nodes are available; Austin has some and will post to the website.

**UQ Repeater – Dahmen**

Scott from the steam plant is willing to work with us putting the repeater on top of the plant.  If we put the repeater in a box they will help us get it mounted to the side of the building on the roof.  We might be able to put the antenna on an offset from one of the stacks or put the antenna on the roof.

**Jet Boat Races**

* Bill will talk to Tim about having DSARC take over the hams at the Lewiston Race.
* Bill and Mike will see who the contact is for the St. Mary’s race on the St. Joe.

**Teaken Butte and Lewiston Hill repeaters – Bart**

* 145.210, - offset
* Teaken – 206.5, out of Peck
* Lewiston – 203.5, top of Lewiston Hill (maybe)
* _The repeaters DO NOT self identify!_  If you use them, each time you identify yourself, also identify as KK6RYR and give the repeater location, Teaken or Lewiston.
