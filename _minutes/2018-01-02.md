---
layout: post
title: '2018-01-02 Meeting Minutes'
date: '2018-01-02 20:00:00'
tags:
- minutes
---

#### DSARC Meeting  
#### January 2, 2018  

** Present: **  
Nick Hewes, AF7ZJ  
Bill Ward, K9GRZ  
Austin Cole, KF7SIW  
Glen Boudreaux, KA5PTG  
Chad Snyder, AE7BZ  
Ty Williams, W7TML  
Heather Cole, KG7BUB  

_Treasurer’s Report_  
$279.03 in the kitty.   $200 used to pay for food (per December meetin)—still have burgers, hot dogs, etc.  

Feel free to pay your dues at your convenience.  Snicker  

_AuxComm Updates_  
Please turn in AuxComm applications when you can.  

_DSARC_  
This weekend, January 6 and 7:  An HF Digital Field Event to make sure we’re ready for Winter Field Day.  Try to get campsites at Robinson Park.  One goal will be to test PAT on HF.  

VHF/UHF digital with PAT:  about 90% for functional, 70% for feature requests.  Still to be complete:  node hopping, forms.  We have it working with the D710s, P2P and via the KF7SIW-10 node.    

PAT should be HF capable, but we haven’t tested it.  

PHARC hasn’t been doing anything for the last several months—other than the Christmas Few.  

_Upcoming Training:_  
Tech Class:  
Lewiston: March 24 and 31.  Location TBD.  
Riggins in June (the 2nd?)  

Repeater etiquette training and a GPS/comms training after the next Tech Class, potentially at Hell’s Gate.   

_Event Updates_  
Boy Scout Klondike Event, January 27.  This conflicts with Winter Field Day but we might be able to combine the two. We’ll bring the trailers, if we can.  Camp Grizzly is not confirmed; Mtn View or Robinson Park will be the alternate.  We’ll plan to run Winter Field Day as 2A. 







