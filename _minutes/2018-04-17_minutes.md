---
layout: post
title: '2018-04-17 Meeting Minutes'
date: '2018-04-18 21:25:00'
tags:
- minutes
---

#### Do Something Amateur Radio Club  
#### Latah County Auxiliary Communications Team  
#### April 17, 2018    

_Present_  
* Nick Hewes, AF7ZJ  
* Bill Ward, K9GRZ  
* Austin Cole, KF7SIW  
Chad Snyder, AE7BZ  
Ty Williams, W7TML  
* Heather Cole, KG7BUB  
Glenn Boudreaux, KA5PTG  
* Bill Magnie, KI7TSV  
* Evan Hart, KG7WPM  
* Tom Storer, KI6DER  
Kelly Blackmon, NA5XX  
* Bill Duncan, KE7OVE  
Christine Berven, KI7UTL  
Micheal Sotolongo, KE7UDV  
Demetre Nicholas, pending  
* Parker Gibson, KI7UTI  
* Dave Schumacher, KF7CSV  
* Pat Blount, KE7SYF  
Autumn St.Amand KI7UTG  
* Bart Gableman N1BAG  

* Present

_Treasurer’s Report_  
* DSARC Kitty:  $365.09  
* AuxComm Kitty:  $315  We need to pay Austin for the web server.  

_Old Business_
After class Field exercise.   Hell’s Gate ARC may need a field exercise in May.  

Tri-State ARO  meeting is sometime.  We should show up as a show of support to that organization.  $15 annual dues, donations for the repeater fund are always welcome.  

Riggins Jet Boat Races April 20 – 22.  
Positions are filled but we can always find a place for extra hams.  

Training opportunities:  

AuxComm May 19 and 20 in Moscow.  ICS 100, 200, 700, 800 and an Amateur Radio license are required to take the class.  See IdahoPrepares.com for registration.  

Oregon ACES.  April 27, 28, 29.  Class will be held at the Clarkston fire department.  See DSARC.US for more information and registration.  

Upcoming projects and exercises.  We need to do some nighttime testing with HF.  We also need to find a way to establish a VHF/UHF link to Spokane and other surrounding areas.    

Event idea:  do a non-repeater exercise to determine who can talk to who on what equipment.  

Field Activities.    

* April 28:  simplex exercise around Moscow area.   Trailer at fair grounds with repeater and simplex.  Dispatch over Moscow Mtn. Repeater to send people to various areas to test simplex.  May be canceled or postponed due to ACES training.  

* May 26:  Lotus Blossom Festival in JK Valley.   We’d be paired up with a CERT member doing traffic control.  Need at least four positions.  

* June 2:  Safety Fair at Eastside Mall.  We need to have interesting activities going on.  We will be co-located with CERT.  

* June 23:  Humane Society of the Palouse 5K, interested in having comms.  It’s also Field Day.  We might be able to support both.  

_New Business_
Member Types:  

Member Type                       | Cost         | Voting Privileges | Slack Access |
|----------------------------------|------------|-------------------|--------------|
| Regular                                | $25          | Full              |  Yes         |
| Family                                | ?            | Full if 18+       |  Yes if 18+  |
| Student College                 | Discounted ? | Full if 18+       | Yes if 18+   |
| Student                                 | Free         | None              | No           |
| Unpaid                                 | $0           | None              | None         |
| Honorary/Complimentary | Free         | None              | Yes          |

Officers:  

President:  Austin  
Vice President:  Heather  

Secretary:  Bill  

Treasurer:  Heather  

PIO:  Nick  

Webmaster:  Austin  

Safety Officer - Handles Safety at events and gatherings  Heather  

Newsletter Editor - Monthly or Bi-Monthly or Quarterly Newsletter that at the very least tells of upcoming events.  Parker  

Web Editor - Handles Web content. Works under the PIO.  

Social Media Coordinator - Handles post to social media. Works under the PIO.  

Membership Coordinator - Handles membership feedback and recruiting. Works under the PIO.  

Facilities Coordinator - Handles reserving and finding locations for events.   Austin and Bill Ward  
