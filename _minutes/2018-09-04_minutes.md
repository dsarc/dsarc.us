---
layout: post
title: '2018-09-04 Meeting Minutes'
date: '2018-09-04 XX:XX:XX'
tags:
- minutes
---

#### Do Something Amateur Radio Club  
#### Latah County Auxiliary Communications Team  
#### September 09, 2018  

**_Attendees_**    
*Nick Hewes, AF7ZJ  
*Bill Ward, K9GRZ  
*Austin Cole, KF7SIW  
*Ty Williams, W7TML  
*Heather Cole, KG7BUB  
*Bill Magnie, KI7TSV  
*Evan Hart, KG7WPM  
*Micheal Sotolongo, KE7UDV  
*Parker Gibson, KI7UTI  
*Dave Schumacher, KF7CSV  
*Autumn St.Amand, KI7UTG  
*Randall Ramsey, KI7BSW  
*Sandee Schumacher, KA9UNA  

* Present

**_Treasurer’s Report_** 
* NA

**_Old Business_**  
Repeater.  AuxComm has been asked to write a proposal for a new repeater to be located on top of a local mountain.  The repeater should include or be capable of analog and DMR voice, APRS, packet messaging, links to other local repeaters.  Evan is leading the effort to write the proposal.  Sooner is better than later.

Parts are in for the portable packet node.  

EVENTS


Sept. 8:  Palouse Sprint Triathlon and Moscow Youth Triathlon.  

Sept. 9:  Moscow Mountain Madness 50k

Sept. 15-16:  Salmon Run, Washington State HF contest

Sept. 22:  Spokane Ham Fest

Oct. 6:  Moscow Cops and Robbers 5/10k Fun Run

Nov. 30:  Moscow Winter Carnival

Idaho Rally:  September 13 – 16 (I think the actual event is 14-16 but you can sign up for specific days, if needed), Placerville, Idaho.  This event takes about 50 hams for full coverage.   Besides doing a  Public Service, you just might get to see some really fast cars going really fast on forest roads!  This year it is sponsored by a national rally organization and there will be several factory teams—meaning more, faster, cars!  Sign up at http://www.rallydata.com/default.cfm

Because the rally is bigger this year, many of the festivities will take place in Idaho City which has more appropriate facilities.  Hams will (probably) still camp at or near Placerville which is also where NCS will be located.

Training opportunities:

Trainings at meetings, date TBD:

Table Top Exercise on how we get deployed.

ICS 100, 200, 700, 800:  What’s in it for me?  Why do we need to take those courses when we don’t seem to use them?

How to fill out the ICS forms required for exercises?

Shoot for a mini-field day in September or October.   We need to line it up to take advantage of a national contest of some sort.  Mike wants to use this as a “mini-SET” and invite other counties to join us for an HF exercise.

Tech Classes are scheduled thru Hell’s Gate ARC.  

We are hosting a tech class September 8 and 9  at the courthouse.   This date was moved from August.

Past Projects and Exercises:


Upcoming/potential projects and exercises.  

We need to do some nighttime testing with HF.  We also need to find a way to establish a VHF/UHF link to Spokane and other surrounding areas.  

Event idea:  do a non-repeater exercise to determine who can talk to who on what equipment.

AuxComm

Emergency dual band radio.  Mike has asked us to install an additional dual band radio in the trailer to be used for monitoring emergency frequencies.  This new radio will be programmed with the local LE freqs and installed such that it is not really easy to remove it.  The new radio will be coupled to a fold over antenna “permanently” mounted on the roof rails for very fast deployment.

Traffic control training.  Mike is asking MPD to provide traffic control training for AuxComm members.  Depending on how long the training is, it may be held at one of our meetings.  Additional trained traffic controllers are need for a variety of events around the county.  

**_New Business_**  
May be getting a new CERT Program Manager, Rob Hooper.
