---
layout: post
title: '2021-01-05 Meeting Minutes'
date: '2021-01-05 00:00:00'
tags:
- minutes
---

### Do Something Amateur Radio Club  
### Latah County Auxiliary Communications Team  
### January 5th, 2021  

#### **_Attendees_**    
* Ty W7TML
* Mike WA7MGN
* Bart N1BAG
* Nick AF7ZJ
* Jim WA7VFQ
* Mike N7ID
* Austin KF7SIW
* David KF7CSV
* Bill KI7TSV
* Sandee KA9UNA
* Dahmen KI7SIJ
* Heather KG7BUB
* Michael KE7UDV

* Present

#### **_Treasurer’s Report_** 
* $979.88

#### **_New Business_**  
**NOTE: Starting in February Discord will replace Slack for chat and video. More details below.** 
* We will be participating in Winter Field Day.
* Austin presented the next part of our series on the Raspberry Pi for Ham radio. He demonstrated installation of applications, OS updates, and some basic Linux commands.
* This was our first meeting using Discord for live streaming the meeting (our meetings are still being done remotely). Video and audio quality were excellent and we will continue using Discord going forward. Discord will also replace Slack for chat, but the latter will be kept open as an archive.
