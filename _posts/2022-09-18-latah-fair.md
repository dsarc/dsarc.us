---
layout: post
title: DSARC at the 2022 Latah County Fair
date: '2022-09-18'
tags:
- 2022, dsarc, auxcomm, events
---

#### Thank you to all who stopped by our booth at the 2022 Latah County Fair!

Some follow up for those who expressed interest in getting licensed or about our upcoming events:

* October 22nd and 23rd we will hold a class for technician licensing followed by a test session on Sunday afternoon. We are working to confirm the location, but it will be in Moscow, Idaho. Class attendance is _not_ required to attend the testing session. A signup for the testing will be posted when we announce the location and hours for the class.

* November 5th and 6th we will host a follow up event for the newly licensed hams and any others who would like to join us at Robinson Park near Moscow. For the new hams we will have a completely hands on simple get-on-the-air orientation which will give people practice communicating over the radio. The new ham orientation event will run on Saturday and maybe Sunday depending on how many people are interested. Times will be posted a little closer to the event.
a
* If you have any questions please reach out to us via our contact page or join us on <a href="https://discord.gg/MfbSu7mweH">Discord</a> We are currently hosting our regular club meetings virtually via Discord on the 1st and 3rd Tuesday of each month. 

Nick
AF7ZJ
