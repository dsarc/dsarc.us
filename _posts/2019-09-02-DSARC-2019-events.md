---
layout: post
title: DSARC 2019 Event Summary
date: '2019-09-02'
tags:
- 2019, dsarc, auxcomm, events
---

DSARC 2019 Event Summary

QST! Here is a list of events for the Do Something Amateur Radio Club in 2019. 


**Amateur Radio activities our club participated in:**
* Winter Field Day - January 26 to 28
    * One of the more popular radio contests started as a means to teach ham radio operators how to operate during cold weather conditions. We operated from Robinson Park in Moscow, ID.
* Growing the amateur radio community
    * New operator training. We supported the University of Idaho ham club during a training exercise for new operators.
* Bi-monthly club meetings
    * We meet on the first and third Tuesday of every month. Our meetings are at the Federal Building in Moscow, ID at 7 p.m.
        * Note: Please arrive ~10 minutes early if possible. The outer dock locks at 5 p.m. and we have to let people in.
* ARRL Field Day - Jun 22-23
    * We spent the weekend at beautiful Spring Valley Reservior located just outside of the Troy, Idaho.


**Events supported by our members:**
* Riggins Jet boat race - April 19th - 21st
    * DSARC/AuxComm members helped staff radio positions.
* Trail Rail Run - June 8th
    * DSARC/AuxComm members helped staff radio positions.
* Paw-louse 5k fun run - June 29th
    * The event is put on the by the Humane Society of the Palouse every year. DSARC/AuxComm members helped staff radio positions.
* Fondo on the Palouse - July 27th
    * This bicycle race, up to 100 miles, is one of our biggest events of the year and it was huge success! DSARC/AuxComm organized communications and we staffed all positions with our members and other hams from local communities.
* Thunder on the Snake - August 23-25
    * For 2019 DSARC/AuxComm organized staffing for all radio positions. This is now our second large event for the year.

**Upcoming events:**  
* Palouse Tri-athlons - September 7th
* Cops and Robbers fun run - September 14th

If you are a ham radio operator, or are interested in becoming one, please contact us about these events. [Contact DSARC](https://dsarc.us/contact/) We can always use extra help!


73,  
Nick - AF7ZJ  
Last updated September 2019    
