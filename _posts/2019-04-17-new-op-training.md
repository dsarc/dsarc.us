---
layout: post
title: New ham radio operator training
date: '2019-04-17 22:00:00'
tags:
- 2019, training, events
---

In our posts and calendar you may have seen new operator training and today I am going to explain what that is. In addition to regular exam sessions we give new radio operators a chance to practice one of the more fun and useful things we do. Talk on the radio! You pick up a phone and start a conversation without even thinking because it long ago became second nature. The difference of using a radio for that purpose requires learning a few new things and we want to speed that process up and teach good radio etiquette in a fun exercise. 

We run the exercises in places like Spring Valley Reservoir, Robinson Park, and the University of Idaho campus. A net control is setup to run the exercise and everyone meets there. New hams are grouped with experienced operators to walk a pre-determined course with locations set with flagging or marked on a map. Upon arriving at a point the ham will check in with net control who will ask for a piece of information related to the location and/or to read off from exercise sheets we have prepared. A similar exchange will be repeated at each location. 

The purpose of these exchanges is to help the new ham learn how communication is handled on a radio net. The experienced ham accompanying them can provide coaching and explain why things are done the way they are. Also, if there are unexpected difficulties the experienced ham can help out. 

This is not limited to licensed hams! If you are interested in ham radio and want to see it in action please join one of the exercises. There is not a set schedule for these exercises since we wait until there is a group of new operators who want the training. We announce trainings several weeks in advance on our website.

If you have questions or wish to express interest in this type of exercise please [contact](https://dsarc.us/contact/) us.