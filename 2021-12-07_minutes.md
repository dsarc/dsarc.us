---
layout: post
title: '2021-12-07 Meeting Minutes'
date: '2021-12-13 00:00:00'
tags:
- minutes
---

### Do Something Amateur Radio Club  
### Latah County Auxiliary Communications Team  
### December 7th, 2021  

#### **_Attendees_**
* Austin KF7SIW
* Mike WA7MGN
* Dave KF7CSV
* Sandee KA9UNA
* Heather KG7BUB
* Bart N1BAG
* Mike N7ID
* Nick AF7ZJ


#### **_Treasurer’s Report_**
* None given

#### **_Old Business_**
* Reminder: Moscow Winter Parade on Thursday. Meet at the Annex building on Almon St. Food for volunteers at 1645 and the parade is 1800-1900.

#### **_New Business_**
* Items
* Meeting on the 21st is canceled.
* Jan 28th to 30 is WFD!!! 
* Officer elections. Officers for 2022 will be as follows:
President - Austin Cole KF7SIW 
Vice President - Nick Hewes AF7ZJ
Secretary - Mike Neelon WA7MGN
Treasurer - Heather Cole KG7BUB
* Latah SAR ground team has invited AuxComm to a meet and greet at their regular meeting on January 12th. Mike, Austin, and Nick will introduce what AuxComm is and our role in the county. 

