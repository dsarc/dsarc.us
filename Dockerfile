FROM ruby

WORKDIR /home/app

ENV PORT 3000

EXPOSE $PORT

RUN gem install bundler
RUN gem install jekyll

# RUN bundle exec jekyll serve --watch

ENTRYPOINT [ "/bin/bash" ]