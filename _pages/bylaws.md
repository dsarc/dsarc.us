---
title: Bylaws
layout: page
permalink: /membership/bylaws/
---

* TOC
{:toc}

# Bylaws of the Do Something Amateur Radio Club, Inc.

## Article I. Offices

> The principal office of the corporation in the state of Idaho shall be
> located in the city of Moscow, county of Latah. The corporation may
> have other offices, either within or outside of the state of Idaho, as
> the board of directors may determine or as the affairs of the
> corporation may require.
>
> The corporation shall have maintain in the state of Idaho a registered
> office, and a registered agent whose office is identical with the
> registered office, as required by the Idaho Nonprofit Corporation Act.
> The registered office may be, but need not be, identical with the
> principal office in the state of Idaho, and the address of the
> registered office may be changed from time to time by the board of
> directors.

## Article II. Members

### Section 1. Classes of Members

> The corporation shall have one class of members.

### Section 2. Election of Members

> Members shall be elected by the board of directors. An affirmative
> vote of 50% of the directors shall be required for election.

### Section 3. Voting Rights

> Each member shall be entitled to one vote on each matter submitted to
> a vote of the members.

### Section 4. Termination of Membership

> The board of directors, by affirmative vote of 2/3 of all of the
> members of the board, may suspend or expel a member for cause after an
> appropriate hearing, and may, by a 2/3 vote of those present at any
> regularly constituted meeting, terminate the membership of any member
> who becomes ineligible for membership, or suspend or expel any member
> who shall be in default in the payment of dues for the period fixed in
> Article XI of these bylaws.

### Section 5. Resignation

> Any member may resign by filing a written resignation with the
> secretary, but resignation shall not relieve the member of the
> obligation to pay any dues, assessments or other charges previously
> accrued and unpaid.

### Section 6. Reinstatement

> Upon written request signed by a former member and filed with the
> secretary, the board of directors may, by the affirmative vote of 75%
> of the members of the board, reinstate the former member to membership
> upon such terms as the board of directors may deem appropriate.

### Section 7. Transfer of Membership

> Membership in this corporation is not transferable or assignable.

## Article III. Meetings of Members

### Section 1. Annual Meeting

> An annual meeting of the members shall be held on the second Tuesday
> in the month of December each year, beginning with the year 2019, at
> the hour of 7:00P.M. for the purpose of electing directors and for the
> transaction of such other business as may come before the meeting. If
> the day fixed for the annual meeting shall be a legal holiday in the
> state of Idaho, the meeting shall be held on the next succeeding
> business day. If the election of directors shall not be held on the
> day designated here for any annual meeting, or at any adjournment of
> the annual meeting, the board of directors shall cause the election to
> be held at a special meeting of the members as soon after as is
> convenient.

### Section 2. Special Meetings

> Special meetings of the members may be called by the president, the
> board of directors, or not less than 75% of the members having voting
> rights.

### Section 3. Place of Meeting

> The board of directors may designate any place, either within or
> outside of the state of Idaho, as the place of meeting for any annual
> meeting or for any special meeting called by the board of directors.
> If no designation is made or if a special meeting be otherwise called,
> the place of meeting shall be the principal office of the corporation
> in the state of Idaho.

### Section 4. Notice of Meetings

> Written or printed notice stating the place, day and hour of any
> meeting of members shall be delivered, either personally, by mail, or
> by electronic mail to each member entitled to vote at such meeting,
> not less than 7 nor more than 30 days before the date of such meeting,
> by or at the direction of the president, or the secretary, or the
> officers or persons calling the meeting. In case of a special meeting
> or when required by statute or by these bylaws, the purposes for which
> the meeting is called shall be stated in the notice. If mailed, the
> notice of a meeting shall be deemed to be delivered when deposited in
> the United States mail addressed to the member at the member's address
> as it appears on the records of the corporation, with postage thereon
> prepaid. Notices delivered by electronic mail shall be deemed
> effective if sent to the members electronic mail address as it appears
> on the records of the corporation. Providing an electronic mail
> address to the corporation shall be deemed to be consent by the member
> to have notices delivered by electronic mail.

### Section 5. Informal Action by Members

> Any action required by law to be taken at a meeting of the members, or
> any action which may be taken at a meeting of members, may be taken
> without a meeting if a consent in writing, setting out the action so
> taken, shall be signed by at least 51% of the members entitled to vote
> with respect to the subject matter of the action.

### Section 6. Quorum

> The members holding 50% of the votes which may be cast at any meeting
> shall constitute a quorum at such meeting. If a quorum is not present
> at any meeting of members, a majority of the members present may
> adjourn the meeting without further notice.

### Section 7. Proxies

> At any meeting of members, a member entitled to vote may vote by proxy
> executed in writing by the member or by his authorized attorney in
> fact. No proxy shall be valid after 11 months from the date of its
> execution, unless otherwise provided in the proxy.

### Section 8. Voting by Mail

> Where directors or officers are to be elected by members or any class
> or classes of members, the election may be conducted by mail in the
> manner that the board of directors shall determine.

## Article IV. Board of Directors

### Section 1. General Powers

> The affairs of the corporation shall be managed by its board of
> directors. Directors need not be residents of the state of Idaho or
> members of the corporation.

### Section 2. Number, Tenure and Qualifications

> The number of directors shall be three. Each director shall hold
> office until the next annual meeting of members and until his
> successor shall have been elected and qualified.

### 

### Section 3. Regular Meetings

> A regular annual meeting of the board of directors shall be held
> without other notice than this bylaw, immediately after, and at the
> same place as, the annual meeting of members. The board of directors
> may provide by resolution the time and place, either within or outside
> of the state of Idaho, for the holding of additional regular meetings
> of the board without other notice than the resolution.

### Section 4. Special Meetings

> Special meetings of the board of directors may be called by or at the
> request of the president or any two directors. The persons authorized
> to call special meetings of the board may fix any place, either within
> or outside of the state of Idaho, as the place for holding any special
> meeting of the board called by them.

### Section 5. Notice

> Notice of any special meeting of the board of directors shall be given
> at least two days previously by written notice delivered personally or
> sent by mail or electronic mail to each director at the address for
> such director as shown by the records of the corporation. If mailed,
> such notice shall be deemed to be delivered when deposited in the
> United States mail in a sealed envelope so addressed, with postage
> prepaid. Any director may waive notice of any meeting. The attendance
> of a director at any meeting shall constitute a waiver of notice of
> such meeting, except where a director attends a meeting for the
> express purpose of objecting to the transaction of any business
> because the meeting is not lawfully called or convened. Neither the
> business to be transacted at, nor the purpose of, any regular or
> special meeting of the board need be specified in the notice or waiver
> of notice of such meeting, unless specifically required by law or by
> these bylaws.

### Section 6. Quorum

> A majority of the board of directors shall constitute a quorum for the
> transaction of business at any meeting of the board; but if less than
> a majority of the directors are present at the meeting, a majority of
> the directors present may adjourn the meeting from time to time
> without further notice.

### Section 7. Manner of Acting

> The act of a majority of the directors present at a meeting at which a
> quorum is present shall be the act of the board of directors, unless
> the act of a greater number is required by law or by these bylaws.

### Section 8. Vacancies

> Any vacancy occurring in the board of directors and any directorship
> to be filled by reason of an increase in the number of directors,
> shall be filled by the board of directors. A director elected to fill
> a vacancy shall be elected for the unexpired term of the director's
> predecessor in office.

### Section 9. Compensation

> Directors as such shall not receive any stated salaries for their
> services, but by resolution of the board of directors a fixed sum and
> expenses of attendance, if any, may be allowed for attendance at each
> regular or special meeting of the board; but nothing contained here
> shall be construed to preclude any director from serving the
> corporation in any other capacity and receiving compensation.

### Section 10. Informal Action by Directors

> Any action required by law to be taken at a meeting of directors, or
> any action which may be taken at a meeting of directors, may be taken
> without a meeting if a consent in writing, setting out the action so
> taken, shall be signed by all of the directors.

## Article V. Officers

### Section 1. Officers

> The officers of the corporation shall be a president, one or more
> vice-presidents (the number to be determined by the board of
> directors), a secretary, a treasurer and such other officers as may be
> elected in accordance with the provisions of this article. The board
> of directors may elect or appoint the other officers, including one or
> more assistant secretaries and one or more assistant treasurers, as it
> shall deem desirable, to have the authority and perform the duties
> prescribed by the board of directors. Any two or more offices may be
> held by the same person.

### Section 2. Election and Term of Office

> The officers of the corporation shall be elected annually by the board
> of directors at the regular annual meeting of the board of directors.
> If the election of officers shall not be held at such meeting, it
> shall be held as soon thereafter as is convenient. New offices may be
> created and filled at any meeting of the board of directors. Each
> officer shall hold office until his or her successor shall have been
> elected and shall have qualified.

### Section 3. Removal

> Any officer elected or appointed by the board of directors may be
> removed by the board of directors whenever in its judgment the best
> interests of the corporation would be served by removal of the
> officer, but such removal shall be without prejudice to the contract
> rights, if any, of the officer so removed.

### Section 4. Vacancies

> A vacancy in any office because of death, resignation, removal,
> disqualification or otherwise, may be filled by the board of directors
> for the unexpired portion of the term.

### Section 5. President

> The president shall be the principal executive officer of the
> corporation and shall in general supervise and control all of the
> business and affairs of the corporation. He or she shall preside at
> all meetings of the members and of the board of directors. He or she
> may sign, with the secretary or any other proper officer of the
> corporation authorized by the board of directors, any deeds,
> mortgages, bonds, contracts, or other instruments which the board of
> directors has authorized to be executed, except in cases where the
> signing and execution shall be expressly delegated by the board of
> directors or by these bylaws or by statute to some other officer or
> agent of the corporation; and in general he or she shall perform all
> duties incident to the office of president and such other duties as
> may be prescribed by the board of directors.

### Section 6. Vice-President

> In the absence of the president or in event of the president's
> inability or refusal to act, the vice-president (or in the event there
> be more than one vice-president, the vice-presidents in the order of
> their election) shall perform the duties of the president, and when so
> acting, shall have all the powers of and be subject to all the
> restrictions upon the president. Any vice-president shall perform such
> other duties as may be assigned to him or her by the president or by
> the board of directors.

### Section 7. Treasurer

> If required by the board of directors, the treasurer shall give a bond
> for the faithful discharge of his or her duties in such sum and with
> such surety as the board of directors shall determine. He or she shall
> have charge and custody of and be responsible for all funds and
> securities of the corporation; receive and give receipts for moneys
> due and payable to the corporation from any source, and deposit all
> such moneys in the name of the corporation in such banks, trust
> companies or other depositories as shall be selected in accordance
> with the provisions of Article VII of these bylaws; and in general
> perform all the duties incident to the office of treasurer and such
> other duties as may be assigned to him or her by the president or by
> the board of directors.

### Section 8. Secretary

> The secretary shall keep the minutes of the meetings of the members
> and of the board of directors in books provided for that purpose; see
> that all notices are given in accordance with the provisions of these
> bylaws or as required by law; be custodian of the corporate records
> and of the seal of the corporation and see that the seal of the
> corporation is affixed to all documents, the execution of which on
> behalf of the corporation under its seal is authorized in accordance
> with the provisions of these bylaws; keep a register of the post
> office address of each member which shall be furnished to the
> secretary by that member; and in general perform all duties incident
> to the office of secretary and such other duties may be assigned by
> the president or by the board of directors.

### Section 9. Assistant Treasurers and Assistant Secretaries

> If required by the board of directors, the assistant treasurers shall
> give bonds for the faithful discharge of their duties in such sums and
> with such sureties as the board of directors shall determine. The
> assistant treasurers and assistant secretaries, in general, shall
> perform the duties assigned to them by the treasurer or the secretary
> or by the president or the board of directors.

## Article VI. Committees

### Section 1. Committees of Directors

> The board of directors, by resolution adopted by a majority of the
> directors in office, may designate and appoint one or more committees,
> each of which shall consist of two or more directors, which
> committees, to the extent provided in the resolution, shall have and
> exercise the authority of the board of directors in the management of
> the corporation; provided, however, that no such committee shall have
> the authority of the board of directors in reference to amending,
> altering or repealing the bylaws; electing, appointing or removing any
> member of any such committee or any director or officer of the
> corporation; amending the articles of incorporation; adopting a plan
> of merger or adopting a plan of consolidation with another
> corporation; authorizing the sale, lease, exchange or mortgage of all
> or substantially all of the property and assets of the corporation;
> authorizing the voluntary dissolution of the corporation or revoking
> proceedings therefor; adopting a plan for the distribution of the
> assets of the corporation; or amending, altering or repealing any
> resolution of the board of directors which by its terms provides that
> it shall not be amended, altered or repealed by the committee. The
> appointment of any such committee and the delegation of authority
> shall not operate to relieve the board of directors of any
> responsibility imposed upon it by law.

### Section 2. Other Committees

> Other committees not having and exercising the authority of the board
> of directors in the management of the corporation may be designated by
> a resolution adopted by a majority of the directors present at a
> meeting at which a quorum is present. Except as otherwise provided in
> the resolution, members of each such committee shall be members of the
> corporation and the president of the corporation shall appoint the
> members of the committees. Any member may be removed by the persons
> authorized to appoint such member whenever in their judgment the best
> interests of the corporation shall be served by such removal.

### Section 3. Term of Office

> Each member of a committee shall continue as a member until the next
> annual meeting of the members of the corporation and until his or her;
> successor is appointed, unless the committee shall be terminated
> sooner, or unless the member be removed from the committee, or unless
> the member shall cease to qualify as a member of the committee.

### Section 4. Chair

> One member of each committee shall be appointed chair by the person or
> persons authorized to appoint the members of the committee.

### Section 5. Vacancies

> Vacancies in the membership of any committee may be filled by
> appointments made in the same manner as provided in the case of the
> original appointments.

### 

### Section 6. Quorum

> Unless otherwise provided in the resolution of the board of directors
> designating a committee, a majority of the whole committee shall
> constitute a quorum and the act of a majority of the members present
> at a meeting at which a quorum is present shall be the act of the
> committee.

### Section 7. Rules

> Each committee may adopt rules for its own government not inconsistent
> with these bylaws or with rules adopted by the board of directors.

## Article VII. Contracts, Checks, Deposits and Funds

### Section 1. Contracts

> The board of directors may authorize any officer or officers, agent or
> agents of the corporation, in addition to the officers so authorized
> by these bylaws, to enter into any contract or execute and deliver any
> instrument in the name of and on behalf of the corporation, and such
> authority may be general or confined to specific instances.

### Section 2. Checks, Drafts, etc.

> All checks, drafts or orders for the payment of money, notes or other
> evidences of indebtedness issued in the name of the corporation, shall
> be signed by those officers or agents of the corporation and in a
> manner as shall be determined by resolution of the board of directors.
> In the absence of this determination by the board of directors, the
> instruments shall be signed by the treasurer or an assistant treasurer
> and countersigned by the president or a vice-president of the
> corporation.

### Section 3. Deposits

> All funds of the corporation shall be deposited to the credit of the
> corporation in the banks, trust companies or other depositories as the
> board of directors may select.

### Section 4. Gifts

> The board of directors may accept on behalf of the corporation any
> contribution, gift, bequest or devise for the general purposes or for
> any special purpose of the corporation.

## Article VIII. Books and Records

> The corporation shall keep correct and complete books and records of
> account and shall also keep minutes of the proceedings of its members,
> board of directors and committees having any of the authority of the
> board of directors, and shall keep at the registered or principal
> office a record giving the names and addresses of the members entitled
> to vote. All books and records of the corporation may be inspected by
> any member, or his agent or attorney for any proper purpose at any
> reasonable time.

## Article IX. Fiscal Year

> The fiscal year of the corporation shall begin on the first day of
> January and end on the last day of December in each year.

## Article X. Dues

### Section 1. Annual Dues

> The board of directors may determine the amount of initiation fee, if
> any, and annual dues payable to the corporation by members of each
> class.

### Section 2. Payment of Dues

> Dues shall be payable in advance on the first day of January in each
> fiscal year. Dues of a new member shall be prorated from the first day
> of the month in which such new member is elected to membership, for
> the remainder of the fiscal year of the corporation.

### Section 3. Default and Termination of Membership

> When any member of any class shall be in default in the payment of
> dues for a period of \[number of months\] months from the beginning of
> the fiscal year or period for which such dues became payable, his or
> her membership may be terminated by the board of directors in the
> manner provided in Article III of these bylaws.

## Article XI. Waiver of Notice

> Whenever any notice is required to be given under the provisions of
> the Idaho Nonprofit Corporation Act or under the provisions of the
> articles of incorporation or the bylaws of the corporation, a waiver
> in writing signed by the persons entitled to the notice, whether
> before or after the time stated there, shall be deemed equivalent to
> the giving of notice.

## Article XII. Amendments to Bylaws

> These bylaws may be altered, amended or repealed and new bylaws may be
> adopted by a majority of the directors present at any regular meeting
> or at any special meeting, if at least two days written notice is
> given of intention to alter, amend or repeal or to adopt new bylaws at
> the meeting.

## Article XIII. Club License Trustee

> The Club Station license (K7DSC) shall be held by a trustee appointed
> by the President who shall hold an amateur license of Technician or
> above.