// function hamPark() {
//  var url = "https://cors-anywhere.herokuapp.com/http://gsx2json.com/api?id=1f7NEbczWSTQQOFwoTgRD4m40tsMI2HkYassFGAsjDd0&sheet=5&columns=false";

//   $(document).ready(function() {
//     $.getJSON(url, function(data){
//       // console.log(data);
//       // Loop through the events in google sheets
//       for(i in data.rows){
//         var start = moment(data.rows[i].startdate).format('MM/DD/YYYY');
//         var end = moment(data.rows[i].enddate).format('MM/DD/YYYY');
//         var park = data.rows[i].park;
//         var nearbytown = data.rows[i].nearbytown;
//         var county = data.rows[i].county;
//         var state = data.rows[i].state;
//         var map = data.rows[i].map;
//         var details = data.rows[i].details;
//         var event = 
//           $('<tr>').append(
//             $('<td>').append(start).attr('id', 'date' + i),
//             $('<td>').append(park),
//             $('<td>').append(nearbytown),
//             $('<td>').append(county),
//             $('<td>').append(state),
//             $('<td>').append('<a href="' + map + '" target="_blank" rel="noopener">' + 'Link' + '</a><br />'),
//             $('<td>').append().attr('id', 'details' + i),
//           );

//         // Auto place events into correct section based on the event date
// 				if(start >= moment().format('MM/DD/YYYY')){
// 					$('#upcomingParks').append(event);
// 				}else{
// 					$('#pastParks').append(event);
//         }
//         // Only show end date if available
//         if(end != 'Invalid date'){
//           $('#date'+i).append(' - ' + end);
//         }
//         // Only show details link if available
//         if(details != null){
//           $('#details'+i).append('<a href="' + details + '" target="_blank" rel="noopener">' + 'Link' + '</a><br />');
//         }

//       }
//     });
//   });
// }

// function testing() {
//  var url = "https://api.palousehosting.com/testing";

//   $(document).ready(function() {
//     $.getJSON(url, function(data){
//       // console.log(data);
//       // Loop through the events in google sheets
//       for(i in data){
        
//         var date = moment(data[i].Date).utc().format('MM/DD/YYYY');
//         var time = moment(data[i].Date).utc().format('HH:mm');
//         var location = data[i].Location;
//         var sponsor = data[i].Sponsor;
//         var map = data[i].Map;
//         var past = data[i].Past;
//         var session = $('<li>').append(`${date} @ ${time} - <a href="${map}" target="_blank" rel="noopener">${location}</a><br /> <strong>Sponsored By:</strong> ${sponsor}`).addClass('list-group-item');

//         // Auto place events into correct section based on the event date
//         if(past != true /*|| date >= moment().utc().format('MM/DD/YYYY')*/){
//           $('#testing').append(session);
//         }
//       }
//     });
//   });
// }

// function classes() {
//  var url = "https://api.palousehosting.com/classes";

//   $(document).ready(function() {
//     $.getJSON(url, function(data){
//       // console.log(data);
//       // Loop through the events in google sheets
//       for(i in data){
//         var event = data[i].Event;
//         var date = moment(data[i]["Start Time"]).format('MM/DD/YYYY');
//         var startTime = moment(data[i]["Start Time"]).format('HH:mm');
//         var endTime = moment(data[i]["End Time"]).format('HH:mm');
//         var location = data[i].Location;
//         var sponsor = data[i].Sponsor;
//         var map = data[i].Map;
//         var flyer = data[i].Flyer;
//         var past = data[i].Past;
//         var session = $('<li>').append(`<strong class="text-center">${event}</strong><br /> <strong>When:</strong> ${date} @ ${startTime} - ${endTime}<br /> <strong>Where:</strong> <a href="${map}" target="_blank" rel="noopener">${location}</a><br /> <strong>Sponsored By:</strong> ${sponsor}<br /> <a href="${flyer}" target="_blank" class="btn btn-default">Flyer</a>`).addClass('list-group-item');

//         // Auto place events into correct section based on the event date
// 		if(date >= moment().utc().format('MM/DD/YYYY') || past !== true){
//           $('#classes').append(session);
//         }

//       }
//     });
//   });
// }

// function nets() {
//  var url = "https://cors-anywhere.herokuapp.com/http://gsx2json.com/api?id=1f7NEbczWSTQQOFwoTgRD4m40tsMI2HkYassFGAsjDd0&sheet=3&columns=false";

//   $(document).ready(function() {
//     $.getJSON(url, function(data){
//       // console.log(data);
//       // Loop through the events in google sheets
//       for(i in data.rows){
//         var net = data.rows[i].net;
//         var day = data.rows[i].day;
//         var time = data.rows[i].time;
//         var purpose = data.rows[i].purpose;
//         var callsign = data.rows[i].callsign;
//         var frequency = data.rows[i].frequency;
//         var offset = data.rows[i].offset;
//         var tone = data.rows[i].tone;
//         var county = data.rows[i].county;
//         var state = data.rows[i].state;
//         var map = data.rows[i].map;
//         var nets = 
//           $('<tr>').append(
//             $('<td>').append(net),
//             $('<td>').append(day + ' ' + time),
//             $('<td>').append(purpose),
//             $('<td>').append(callsign),
//             $('<td>').append(frequency),
//             $('<td>').append(offset),
//             $('<td>').append(tone),
//             $('<td>').append(county),
//             $('<td>').append(state)
//           );

//         $('#nets').append(nets);

//       }
//     });
//   });
// }

// function repeaters() {
//   var url =
//     "https://cors-anywhere.herokuapp.com/http://gsx2json.com/api?id=1f7NEbczWSTQQOFwoTgRD4m40tsMI2HkYassFGAsjDd0&sheet=2&columns=false";

//   $(document).ready(function() {
//     $.getJSON(url, function(data) {
//       // console.log(data);
//       // Loop through the events in google sheets
//       for (i in data.rows) {
//         var callsign = data.rows[i].callsign;
//         var frequency = data.rows[i].frequency;
//         var offset = data.rows[i].offset;
//         var tone = data.rows[i].tone;
//         var links = data.rows[i].links;
//         var purpose = data.rows[i].purpose;
//         var capabilities = data.rows[i].capabilities;
//         var county = data.rows[i].county;
//         var state = data.rows[i].state;
//         var repeaters = $("<tr>").append(
//           $("<td>").append(callsign),
//           $("<td>").append(frequency),
//           $("<td>").append(offset),
//           $("<td>").append(tone),
//           $("<td>").append(links),
//           $("<td>").append(purpose),
//           $("<td>").append(capabilities),
//           $("<td>").append(county),
//           $("<td>").append(state)
//         );

//         $("#repeaters").append(repeaters);
//       }
//     });
//   });
// }

// function reports() {
//  var url = "https://cors-anywhere.herokuapp.com/http://gsx2json.com/api?id=1f7NEbczWSTQQOFwoTgRD4m40tsMI2HkYassFGAsjDd0&sheet=1&columns=false";

//   $(document).ready(function() {
//     $.getJSON(url, function(data){
//         var data = data.rows.reverse();
//       $.each(data, function(i, value) {
//         var event = this.event;
//         var date = this.date;
//         var report = this.report;
//         var group = this.group;
//         if (group == 'both' || group == 'dsarc') {
//           $('<tr>').append(
//             $('<td>').append(event + '<br />'),
//             $('<td>').append(date + '<br />'),
//             $('<td>').append('<a href="' + report + '" target="_blank" rel="noopener">' + 'Link' + '</a><br />'),
//           ).appendTo('#report');
//         }
//       });
//     });
//   });
// }