// Month View Google Calendar
function month(){
  $(document).ready(function() {
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,listMonth'
      },
      defaultView: 'month',
      googleCalendarApiKey: 'AIzaSyDyMoDpMndtgfhh62g_B9Gr5YrXhSBMhmI',
      eventSources: [
        {
          //DSARC
          googleCalendarId: 'pkr1feh376liutlgs84gd0bjmo@group.calendar.google.com',
          color: '#737373',
          textColor: '#FFFFFF'
        },
        {
          // W7UQ
          googleCalendarId: 'ffn71ed4611udadmfc6bngrdao@group.calendar.google.com',
          color: '#000000',
          textColor: '#FFDF00'
        },
        {
          //Aux Comm
          googleCalendarId: 'la31li46n7q6j2ajoi6ki2p8d0@group.calendar.google.com',
          color: '#3a87ad',
          textColor: '#FFFFFF'
        },
        {
          //PHARC
          googleCalendarId: 'gkrjmuv8ik6dif1p9ct0efpbo0@group.calendar.google.com',
          color: '#f4a742',
          textColor: '#000000'
        }
      ],
      eventClick: function(event) {
        // opens events in a popup window
        window.open(event.url, 'gcalevent', 'width=700,height=600');
        return false;
      },
      loading: function(bool) {
        $('#loading').toggle(bool);
      }
    });
  });
}

// Upcoming Events View Google Calendar
function upComing(){
  $(document).ready(function() {
    $('#upcoming').fullCalendar({
      header: {
        left: 'false',
        center: 'false',
        right: 'false'
      },
      defaultView: 'list',
      visibleRange: function(currentDate) {
        return {
          start: currentDate.clone().subtract(1, 'days'),
          end: currentDate.clone().add(1, 'months') // exclusive end, so 3
        };
      },
      height: 150,
      eventClick: function(event) {
        window.open(event.url, 'gcalevent', 'width=700,height=600');
        return false;
      },
      googleCalendarApiKey: 'AIzaSyDyMoDpMndtgfhh62g_B9Gr5YrXhSBMhmI',
      eventSources: [
        {
          //DSARC
          googleCalendarId: 'pkr1feh376liutlgs84gd0bjmo@group.calendar.google.com',
          color: '#737373',
          textColor: '#FFFFFF'
        },
        {
          //Aux Comm
          googleCalendarId: 'la31li46n7q6j2ajoi6ki2p8d0@group.calendar.google.com',
          color: '#3a87ad',
          textColor: '#FFFFFF'
        },
        {
          // W7UQ
          googleCalendarId: 'ffn71ed4611udadmfc6bngrdao@group.calendar.google.com',
          color: '#000000',
          textColor: '#FFDF00'
        },
        {
          //PHARC
          googleCalendarId: 'gkrjmuv8ik6dif1p9ct0efpbo0@group.calendar.google.com',
          color: '#f4a742',
          textColor: '#000000'
        }
      ],
      loading: function(bool) {
        $('#loading').toggle(bool);
        $('.fc-toolbar').remove();
      },
    });
  });
}
