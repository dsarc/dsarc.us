function repeaters() {
  var url = "https://api.dsarc.us/Repeaters";

  $(document).ready(function () {
    $.getJSON(url, function (data) {
      // console.log(data);
      // Loop through the events in google sheets
      for (i in data) {
        var callsign = data[i].Callsign;
        var frequency = data[i].Frequency;
        var offset = data[i].Offset;
        var tone = data[i].Tone;
        var links = data[i].Links;
        var purpose = data[i].Purpose;
        var capabilities = data[i].Capabilities;
        var county = data[i].County;
        var state = data[i].State;
        var repeaters = $("<tr>").append(
          $("<td>").append(callsign),
          $("<td>").append(frequency),
          $("<td>").append(offset),
          $("<td>").append(tone),
          $("<td>").append(links),
          $("<td>").append(purpose),
          $("<td>").append(capabilities),
          $("<td>").append(county),
          $("<td>").append(state)
        );

        $("#repeaters").append(repeaters);
      }
    });
  });
}

function nets() {
  var url = "https://api.dsarc.us/Nets";

  $(document).ready(function () {
    $.getJSON(url, function (data) {
      // console.log(data);
      // Loop through the events in google sheets
      for (i in data) {
        var net = data[i].Name;
        var day = data[i].Day;
        var time = data[i].Time;
        var purpose = data[i].Purpose;
        var callsign = data[i].Callsign;
        var frequency = data[i].repeaters.Frequency;
        var offset = data[i].repeaters.Offset;
        var tone = data[i].repeaters.Tone;
        var county = data[i].repeaters.County;
        var state = data[i].repeaters.State;
        // var map = data[i].map;
        var nets =
          $('<tr>').append(
            $('<td>').append(net),
            $('<td>').append(day + ' ' + time),
            $('<td>').append(purpose),
            $('<td>').append(callsign),
            $('<td>').append(frequency + " MHz"),
            $('<td>').append(offset + " MHz"),
            $('<td>').append(tone + " Hz"),
            $('<td>').append(county),
            $('<td>').append(state)
          );

        $('#nets').append(nets);

      }
    });
  });
}

function testing() {
  var url = "https://api.dsarc.us/Testings?_sort=Date:ASC";

  $(document).ready(function () {
    $.getJSON(url, function (data) {
      // console.log(data);
      // Loop through the events in google sheets
      for (i in data) {

        var date = moment(data[i].Date);
        var day = moment(date).utc().format('MM/DD/YYYY');
        var time = moment(date).utc().format('HH:mm');
        var location = data[i].Location;
        var sponsor = data[i].Sponsor;
        var map = data[i].Map;
        var registration = data[i].Registration;
        var session = $('<li>').append(`${day} @ ${time} - <a href="${map}" target="_blank" rel="noopener">${location}</a><br /> <strong>Sponsored By:</strong> ${sponsor}<br /><a href="${registration}" target="_blank" class="btn btn-default">Register</a>`).addClass('list-group-item');
        
        // Debug Time Compare
        // console.log(`${date} is before ${moment(date).isBefore()}`);

        // Auto place events into correct section based on the event date
        if (!moment(date).isBefore()) {
          $('#testing').append(session);
        }
      }
    });
  });
}

function classes() {
  var url = "https://api.dsarc.us/Classes?_sort=Date:ASC";

  $(document).ready(function () {
    $.getJSON(url, function (data) {
      // console.log(data);
      // Loop through the events in google sheets
      for (i in data) {
        var event = data[i].Event;
        var date = moment(data[i]["Start Time"]);
        var day = moment(date).utc().format('MM/DD/YYYY');
        var startTime = moment(date).utc().format('HH:mm');
        var endTime = moment(data[i]["End Time"]).utc().format('HH:mm');
        var location = data[i].Location;
        var sponsor = data[i].Sponsor;
        var map = data[i].Map;
        var flyer = data[i].Flyer;
        var show = data[i].Show;

        // Debug Time Compare
        // console.log(`${date} is before ${moment(date).isBefore()}`);
        // var past = data[i].Past;
        var session = $('<li>').append(`<strong class="text-center">${event}</strong><br /> <strong>When:</strong> ${day} @ ${startTime} - ${endTime}<br /> <strong>Where:</strong> <a href="${map}" target="_blank" rel="noopener">${location}</a><br /> <strong>Sponsored By:</strong> ${sponsor}<br /> <a href="${flyer}" target="_blank" class="btn btn-default">Flyer</a>`).addClass('list-group-item');

        // Auto place events into correct section based on the event date
        if (!moment(date).isBefore() && show) {
          $('#classes').append(session);
        }

      }

    });
  });
}