---
layout: post
title: 'April 2018 ACES Class'
eventDate: '2018-04-10 19:37:00'
eventType: 'class'
past: true
---

## What

This is a class that will teach basic emergency communications topics and procedures for operating VHF/UHF voice.

## When
*Times are subject to change*

| Date     | Starts | Ends |
|----------|--------|------|
| April 27 | Evening| TBD  |
| April 28 | All Day| TBD  |
| April 29 | Half Day - Morning| TBD  |
{:.table .table-striped .table-bordered}

## Where
Clarkston Fire Department

820 5th St, Clarkston, WA 99403

[Directions](https://goo.gl/maps/7t77BrVUsZo){:.btn .btn-default target="_blank"}

## More Info

More info can be found [here](http://www.oregonaces.org/content/basic-certification){:target="_blank"}.

## Signup Here

<script src="https://static.airtable.com/js/embed/embed_snippet_v1.js"></script><iframe class="airtable-embed airtable-dynamic-height" src="https://airtable.com/embed/shrvT7nQRJzA9E5Qn?backgroundColor=blue" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>