---
layout: post
title: Tech License Class
lastUpdate: 2022-10-18T18:36:33.497Z
eventDate: 2022-10-22T02:37:00.000Z
eventType: class
past: true
tags: class
---
- - -

**What:** DSARC and Latah Auxiliary Communications Team will be hosting a Technician License Class. This class will be a weekend crash course that will get you the knowledge to pass the test. There will be a testing session at the end of the class to ready to get on the air.

**Why:** To talk to other hams around the world, use in emergency situations where you do not have cell phone signal, or simply to use in rural areas that are always outside of cell signal.

**When:** October 22-23 from 0900-1700

**Where:** Latah County Fair Grounds - Grange Building [(Click here for map and directions)](https://goo.gl/maps/eGwL6qnyLGyCyWGA7){:target="_blank"}

**How Much:** Nothing. That's right we don't charge for the class or the testing sessions. We offer the areas only free testing sessions, saving you at least $15. We also have the fastest turnaround time in the area for you getting your license.

**Have More Questions:** No problem contact us [here](/contact/).

<!-- Noscript content for added SEO -->
<noscript><a href="https://www.eventbrite.com/e/amateur-radio-tech-license-class-tickets-444379299447" rel="noopener noreferrer" target="_blank">Register for Class</a></noscript>
<!-- You can customize this button any way you like -->
<button id="eventbrite-widget-modal-trigger-444379299447" type="button">Register for Class</button>

<script src="https://www.eventbrite.com/static/widgets/eb_widgets.js"></script>

<script type="text/javascript">
    var exampleCallback = function() {
        console.log('Order complete!');
    };

    window.EBWidgets.createWidget({
        widgetType: 'checkout',
        eventId: '444379299447',
        modal: true,
        modalTriggerElementId: 'eventbrite-widget-modal-trigger-444379299447',
        onOrderComplete: exampleCallback
    });
</script>

<!-- <iframe class="airtable-embed" src="https://airtable.com/embed/shrvT7nQRJzA9E5Qn?backgroundColor=blue" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe> -->
