---
layout: post
title: Thunder on the Snake
eventDate: '2018-04-10 19:37:00'
eventType: 'event'
past: true
---

---

**What:**
Provide Radio Support for the Thunder on the Snake jet boat races.

**When:** August 25-26

**Where:** Lewiston, Idaho

**Have More Questions:**

No problem contact us [here](/contact/).

[Sign Up](https://goo.gl/forms/nCDobUcKq9Iznn2O2){: .btn-lg .btn-success}

